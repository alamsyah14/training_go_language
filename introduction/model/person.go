package model

//model person
type Person struct {
	PID     int
	Name    string
	Age     int
	Address string
}
