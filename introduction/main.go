package main

import (
	/*
		"training_go/training_go_language/introduction/function"
		"training_go/training_go_language/introduction/handler"
	*/
	"fmt"
	"strings"
)

/* Method */
/*
type pelajar struct {
	id   int
	name string
}

func (p pelajar) sayHello() {
	fmt.Println("Hallo", p.name)
}

func (p pelajar) getName(posisi int) string {
	return strings.Split(p.name, " ")[posisi-1]
}

func (p pelajar) chageNameOne(name string) {
	fmt.Println("---> on chageNameOne, name changed to", name)
	p.name = name
}

func (p *pelajar) chageNameTwo(name string) {
	fmt.Println("---> on chageNameTwo, name changed to", name)
	p.name = name
}
*/

/*interface*/
/*
type hitung interface {
	luas() float64
	keliling() float64
}

type lingkaran struct {
	diameter float64
}

func (l lingkaran) jariJari() float64 {
	return l.diameter / 2
}

func (l lingkaran) luas() float64 {
	return math.Pi * math.Pow(l.jariJari(), 2)
}

func (l lingkaran) keliling() float64 {
	return math.Pi * l.diameter
}

type segitiga struct {
	alas   float64
	tinggi float64
}

func (s segitiga) luas() float64 {
	return (s.alas * s.tinggi) / 2
}

func (s segitiga) keliling() float64 {
	return s.alas * 3
}
*/

/*interface empty*/
type person struct {
	age  int
	name string
}

func main() {
	/*
		var namaawal, namatenggah, namaakhir string
		namaawal = "Muhammad"
		namatenggah = " Alamsyah "
		namaakhir = "Putra"
		var nama = namaawal + namatenggah + namaakhir
		var nilai int = 10

		function.Cetak(nilai, nama)

		returnval := function.Returnvalue(true)

		fmt.Println(returnval)
	*/
	/*
		var tipe, value1, value2 int
		tipe = 3
		value1 = 100
		value2 = 1000
		code, message := function.Multiplevaluereturn(tipe, value1, value2)

		if code == 200 {
			fmt.Println(message)
		} else {
			fmt.Println(message)
		}

		ratarata := function.Variadic(6, 4, 10, 5, 100, 20)

		fmt.Printf("Hasil : %f \n", ratarata)

		function.Mainpointer()
		var number int = 15
		fmt.Println("before :", number)
		fmt.Println("before (address) :", &number)
		function.Pointerchange(&number, 100)
		fmt.Println("after  :", number)
		fmt.Println("after (address) :", &number)

		create := handler.Create()
		fmt.Printf("Create : %v \n", create)
		edit := handler.Edit(create)
		fmt.Printf("Edit : %v \n", edit)
	*/
	/*method*/
	/*
		pel := pelajar{1, "Muhammad Alamsyah Putra"}
		pel.sayHello()

		var name = pel.getName(3)
		fmt.Println("Nama Pangillan : ", name)
		fmt.Println("Pelajar before", pel.name)

		pel.chageNameOne("Json Post")
		fmt.Println("Pelajar after chageNameOne", pel.name)

		pel.chageNameTwo("Post")
		fmt.Println("Pelajar after chageNameTwo", pel.name)

		methPointerOne := pelajar{2, "Jhon Doe"}
		methPointerOne.sayHello()

		methPointerTwo := &pelajar{2, "Jhon Doe"}
		methPointerTwo.sayHello()
	*/
	/*interface*/
	/*
		var rumus hitung

		rumus = lingkaran{14.0}
		fmt.Println("===== lingkaran")
		fmt.Println("luas      :", rumus.luas())
		fmt.Println("keliling  :", rumus.keliling())
		fmt.Println("jari-jari :", rumus.(lingkaran).jariJari())

		rumus = segitiga{5.0, 3.0}
		fmt.Println("===== segitiga")
		fmt.Println("luas      :", rumus.luas())
		fmt.Println("keliling  :", rumus.keliling())
	*/

	/*interface empty*/
	var inter interface{}

	inter = "Alamsyah"
	fmt.Println(inter)

	inter = []string{"one", "two", "three"}
	fmt.Println(inter)

	inter = 12.5
	fmt.Println(inter)

	inter = map[string]interface{}{
		"name":      "Muhammad Alamsyah Putra",
		"grade":     1,
		"breakfast": []string{"Egs", "Meat", "Banana"},
	}
	fmt.Println(inter)

	inter = []string{"one", "two", "three"}
	var number = strings.Join(inter.([]string), ", ")
	fmt.Println(number, "is my favorite Number")

	inter = &person{33, "Muhammad Alamsyah Putra"}
	fmt.Println(inter)
	var nama = inter.(*person).age
	fmt.Println(nama)

	var personal = []map[string]interface{}{
		{"name": "Wick", "age": 23},
		{"name": "Ethan", "age": 23},
		{"name": "Bourne", "age": 22},
	}

	for _, each := range personal {
		fmt.Println(each["name"], "age is", each["age"])
	}

	var fruits = []interface{}{
		map[string]interface{}{"name": "strawberry", "total": 10},
		[]string{"manggo", "pineapple", "papaya"},
		"orange",
	}

	for _, each := range fruits {
		fmt.Println(each)
	}
}
