package handler

import (
	"training_go/training_go_language/introduction/model"
)

func Create() model.Student {
	studen := &model.Student{}
	person := &model.Person{
		PID:     1,
		Name:    "Alamsyah",
		Age:     32,
		Address: "Jl. Kramatjati Jakarta timur",
	}
	studen.ID = 1
	studen.Person = *person

	return *studen
}

func Edit(studen model.Student) model.Student {
	person := &model.Person{
		PID:     3,
		Name:    "Muhammad Alamsyah Putra",
		Age:     35,
		Address: "Jl. Kramatjati -  Jakarta timur",
	}
	studen.ID = 2
	studen.Person = *person
	return studen
}
