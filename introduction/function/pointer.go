package function

import "fmt"

func Mainpointer() {
	var valueA int = 10
	var valueB *int = &valueA

	fmt.Println("numberA (value)   :", valueA)
	fmt.Println("numberA (address) :", &valueA)

	fmt.Println("numberB (value)   :", *valueB)
	fmt.Println("numberB (address) :", valueB)
}

func Pointerchange(original *int, value int) {
	*original = value
}
