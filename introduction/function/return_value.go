package function

import "strconv"

func Returnvalue(p bool) string {
	var a string
	if p {
		a = "Sukses"
	} else {
		a = "Gagal"
	}
	return a
}

func Multiplevaluereturn(tipe int, val1 int, val2 int) (code int, message string) {
	var result int
	switch tipe {
	case 1:
		result = val1 + val2
	case 2:
		result = val1 - val2
	case 3:
		result = val1 / val2
	case 4:
		result = val1 * val2
	}

	if result > 0 {
		code = 200
		message = "Hasil perhitungan = " + strconv.Itoa(result)
	} else {
		code = 401
		message = "Error, hasil perhitungan kurang dari nol"
	}
	return
}

func Variadic(val ...int) float32 {
	var total int
	for _, x := range val {
		total += x
	}
	average := float32(total / len(val))
	return average
}
